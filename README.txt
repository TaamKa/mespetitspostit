L'application possède toutes les fonctionnalités de bases du cahier des charges, à savoir :
    - Un écran avec la liste des tâches. Un bouton en bas de cet écran permet d'ajouter une nouvelle tâche en 2 temps : d'abord ajouter un titre à la tâche, puis le message à sauvegarder
    - Une tâche est répertoriée sur l'écran principale avec kes autres (si d'autres existent) sous forme de liste, avec une ligne comprenant le titre et un résumé de la tâche
    - Lorsqu'on appuie sur une tâche, on arrive sur un écran qui affiche le texte complet de la tâche, ainsi que divers bouton :
        - La modification de statut : lorsqu'il est pressé, l'utilisateur est renvoyé vers l'écran principal, et peut remarquer la tâche modifiée par sous surlignement rouge.
        - La modification du texte : lorsqu'il est pressé, le bouton ouvre une modal permettant de modifier le message de la tâche. Le résumé de la tâche est mis à jour si nécessaire.
        - Le suppression de la tâche : seulement disponible pour les tâches notifiées 'Fait', ce bouton supprime la tâche et renvoi l'utilisateur vers l'écran principal
        - Le bouton de retour : renvoi simplement l'utilisateur vers l'écran principal


Gestion des erreurs :
    - Lorsqu'un écran de saisie de texte (titre, message ou modification de message) est validé sans texte, une modal d'alerte apparait pour signifier le message vide et replace l'utilisateur devant le même écran de saisie de textee
    - Lorsqu'un écran de saisie de texte (titre, message ou modification de message) est annulé, l'état de la tâche (inexistante ou à l'état avant demande de modification) est récupéré, et aucune modification n'est enregistrée$


Bonus :
Aucun bonus n'a été développé dans cet application
